<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tugas Harian 1 - Pekan 1</title>
</head>
<body>
    <header>
        <div>
            <h1>SanberBook</h1>
            <h2>Social Media Developer Santai Berkualitas</h2>
            <p>Belajar dan berbagi agar hidup ini semakin santai berkualitas</p>
        </div>
    </header>
    <main>
        <div>
            <h3>Benefit Join di SanberBook</h3>
            <ul>
                <li>Mendapatkan motivasi dari sesama developer</li>
                <li>Sharing knowledge dari para mastah Sanber</li>
                <li>Dibuat oleh calon web developer terbaik</li>
            </ul>
            <h3>Cara Bergabung ke Sanberbook</h3>
            <ol>
                <li>Mengunjungi website ini</li>
                <li>Mendaftar di <a href="/register"> Form Sign Up </a></li>
                <li>Selesai!</li>
            </ol>
        </div>
    </main>
</body>
</html>