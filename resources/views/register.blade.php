<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Halaman Form</title>
</head>
<body>
    <header>
        <div>
            <h1>Buat Account Baru!</h1>
        </div>
    </header>
    <main>
        <div>
            <form action="/welcome" method="POST">
                @csrf
                <h3>Sign Up Form</h3>
                <p>First name :</p>
                <input type="text" name="firstName">
                <p>Last name :</p>
                <input type="text" name="lastName">
                <p>Gender :</p>
                <input type="radio" name="gender"> Male <br>
                <input type="radio" name="gender"> Female <br>
                <input type="radio" name="gender"> Other <br>
                <p>Nationality :</p>
                <select name="nationality_user" id=""> <br>
                    <option value="0">Indonesian</option>
                    <option value="1">Singaporian</option>
                    <option value="2">Malaysian</option>
                    <option value="3">Australian</option>
                </select>
                <p>Language Spoken :</p>
                <input type="checkbox"> Bahasa Indonesia <br>
                <input type="checkbox"> English <br>
                <input type="checkbox"> Other <br>
                <p>Bio :</p>
                <textarea name="" id="" cols="30" rows="10"></textarea> <br>
                <input type="submit" value="Sign Up"></>
            </form>
                

        </div>
    </main>
</body>
</html>